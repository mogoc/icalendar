package icalendar

/*

type ICalStream struct {
	Objects []ICalBody `vcf:"VCALENDAR"`
}

type ICalObject struct {
	// MUST ONCE
	ProdID  `vcf:"PRODID"`
	Version `vcf:"VERSION"`

	// MAY ONCE
	Calscale `vcf:"CALSCALE"`
	Method   `vcf:"METHOD"`

	// MAY MULTIPLE
	IANAProp
	XProp

	// Components
	Events     []Event    `vcf:"VEVENT"`
	Todos      []Todo     `vcf:"VTODO"`
	Journals   []Journal  `vcf:"VJOURNAL"`
	Freebusies []FreeBusy `vcf:"VFREEBUSY"`
	Timezones  []Timezone `vcf:"VTIMEZONE"`
	IANAComps  []IANAComp
	XComps     []XComp
}

type Event struct {
	// ONCE
	DTStamp
	UID
	// MAY ONCE
	DTStart
	// MAY ONCE
	Class
	Created
	Description
	Geo
	LastMod
	Location
	Organzizer
	Priority
	Seq
	Status
	Summary
	Transp
	URL
	RecurID
	// MAY but should only once
	RRule
	// eiter or
	DTend
	Duration
	// Optional multiple
	Attach
	Attendee
	Categories
	Comment
	Contact
	Exdate
	RStatus
	Related
	Resources
	RDate
	XProp
	IanaProp
}

type ToDo struct {
	// Must once
	DTStamp
	UID
	// Optional once
	Class
	Completed
	Created
	Description
	DTStart
	Geo
	LastMod
	Location
	Organizer
	Percent
	Priority
	RecurID
	Seq
	Status
	Summary
	URL
	// Optional should only once
	RRule
	// either or
	Due
	Duration
	// Optional multiple
	Attach
	Attendee
	Categories
	Comment
	Contact
	Exdate
	RStatus
	Related
	Resources
	RDate
	XProp
	IANAProp
}

type Journal struct {
	// must once
	DTStamp
	UID
	// optional once
	Class
	Created
	DTStart
	LastMod
	Organizer
	RecurID
	Seq
	Status
	Summary
	URL
	// may but should only once
	RRule
	// optinal multiple
	Attach
	Attendee
	Categories
	Comment
	Contact
	Description
	ExDate
	Related
	RDate
	RStatus
	XProp
	IANAProp
}

type FreeBusy struct {
	// must once
	DTStamp
	UID
	// optional once
	Contact
	DTStart
	DTEnd
	Organizer
	URL
	// optional multiple
	Attendee
	Comment
	Freebusy
	RStatus
	XProp
	IANAProp
}

type Timezone struct {
	// must once
	tzid
	// optional once
	LastMod
	TZUrl
	// at least one of them
	Standardc
	Daylightc
	// Optional multiple
	XProp
	IANAProp
}

type Standardc struct {
	// must once
	DTStart
	TZOffsetTo
	TZOffsetFrom
	// optional multiple should only once
	RRule
	COMMENT
	RDATE
	TZNAME
	XProp
	IANAProp
}

type Daylightc struct {
	// must once
	DTStart
	TZOffsetTo
	TZOffsetFrom
	// optional multiple should only once
	RRule
	COMMENT
	RDATE
	TZNAME
	XProp
	IANAProp
}

type Alarm struct {
	// exactly one of them
	Audio
	Display
	Email
}

type Audio struct {
	// must once
	Action
	Trigger
	// optional together
	Duration
	Repeat
	// optional once
	Attach
	// optional multi
	XProp
	IANAProp
}

type Display struct {
	// must once
	Action
	Description
	Trigger
	// optional together
	Duration
	Repeat
	// optional multi
	XProp
	IANAProp
}

type Email struct {
	// must once
	Action
	Description
	Trigger
	Summary
	// required
	Attendee
	// optional together
	Duration
	Repeat
	// optional multi
	Attach
	XProp
	IANAProp
}
*/
