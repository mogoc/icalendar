package linefold

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func testFile(t *testing.T, path string) {
	f, err := os.Open(path)
	assert.NoError(t, err)
	defer f.Close()

	r1 := NewReader(f)
	expected, err := ioutil.ReadAll(r1)
	assert.NoError(t, err)
	out := bytes.NewBuffer(make([]byte, 0))
	w := NewWriter(out)
	_, err = w.Write(expected)
	assert.NoError(t, err)
	r2 := NewReader(out)
	actual, err := ioutil.ReadAll(r2)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestUsingTestData(t *testing.T) {
	filepath.Walk("../testdata/", func(path string, info os.FileInfo, err error) error {
		if !strings.Contains(info.Name(), "_incorrect") && (filepath.Ext(path) == ".ics" || filepath.Ext(path) == ".ifb") {
			t.Run(info.Name(), func(t *testing.T) {
				testFile(t, path)
			})
		}
		return nil
	})
}

func testContinousStream(t *testing.T, r io.Reader) {
	pr, pw := io.Pipe()
	ref, pw2 := io.Pipe()
	r2 := NewReader(pr)
	w := bufio.NewWriterSize(pw2, 500*1024)
	go func() {
		b := make([]byte, 8*1024)
		wr := bufio.NewWriterSize(NewWriter(pw), 500*1024)
		for {
			n, err := r.Read(b)
			assert.NoError(t, err)
			_, err = wr.Write(b[:n])
			assert.NoError(t, err)
			_, err = w.Write(b[:n])
			assert.NoError(t, err)
			var wg sync.WaitGroup
			wg.Add(1)
			go func() {
				for w.Available() < len(b) {
					w.Flush()
				}
				wg.Done()
			}()
			for wr.Available() < len(b) {
				wr.Flush()
			}
			wg.Wait()
		}
	}()
	buf1 := make([]byte, 80)
	buf2 := make([]byte, 80)
	for i := 0; i < 10000 && !t.Failed(); i++ {
		n := rand.Intn(81)
		_, err1 := io.ReadFull(r2, buf1[:n])
		_, err2 := io.ReadFull(ref, buf2[:n])
		if assert.Equal(t, err2, err1) {
			assert.Equal(t, buf2[:n], buf1[:n])
		}
	}
}

func TestRandom(t *testing.T) {
	testContinousStream(t, rand.New(rand.NewSource(0)))
}
