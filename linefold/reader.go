package linefold

import (
	"bytes"
	"io"
)

type reader struct {
	r   io.Reader
	buf bytes.Buffer
}

func (r *reader) Read(p []byte) (n int, err error) {
	// Ignore empty reads
	if len(p) == 0 {
		return
	}
	// Clear the already processed buffer
	if r.buf.Len() >= len(p) && r.buf.Len() > 3 {
		return r.buf.Read(p)
	}
	for {
		lr := &io.LimitedReader{
			R: r.r,
			N: 3 + int64(len(p)) - int64(r.buf.Len()),
		}
		_, err = r.buf.ReadFrom(lr)
		// detect EOF from source
		if err == nil && lr.N > 0 {
			err = io.EOF
		}

		// replace pattern in slice
		b := r.buf.Bytes()
		match := 0
		for i := 0; i < len(b); i++ {
			if b[i] == '\r' {
				match = 1
			} else if match == 1 && b[i] == '\n' {
				match = 2
			} else if match == 2 && (b[i] == '\t' || b[i] == ' ') {
				match = 0
				copy(b[i-2:], b[i+1:])
				b = b[:len(b)-3]
				i -= 3 // Start over at i-2
			} else {
				match = 0
			}
		}

		// if an error occured (no more data will follow) or the
		// buffer is long enough to handle the remaining pattern
		// next time, release the data
		r.buf.Truncate(len(b))
		if err != nil || len(b)-match >= len(p) {
			n, _ = r.buf.Read(p)
			return
		}
	}
}

// NewReader wraps an io.Reader to automatically unfold lines as
// described in https://tools.ietf.org/html/rfc5545#section-3.1.
// This is achieved by essentialy removing the sequences "\r\n "
// and "\r\n\t" from the data stream.
func NewReader(r io.Reader) io.Reader {
	return &reader{
		r: r,
	}
}
