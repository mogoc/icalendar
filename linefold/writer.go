package linefold

import (
	"io"
)

type writer struct {
	w io.Writer
	n int
}

// Write writes the given byte slice to the wrapped io.Writer.
func (w *writer) Write(p []byte) (n int, err error) {
	var i int
	var c int
	for _, b := range p {
		c++
		if b == '\r' || b == '\n' {
			w.n = 0
			continue
		}
		w.n++
		if w.n >= 75 {
			i, err = w.w.Write(p[n : n+c])
			n += i
			if err != nil {
				return
			}
			_, err = w.w.Write([]byte("\r\n "))
			if err != nil {
				return
			}
			c = 0
			w.n = 0
		}
	}
	if n != len(p) {
		i, err = w.w.Write(p[n:])
		n += i
	}
	return
}

// NewWriter wraps an io.Writer to automatically fold long lines
// as described in https://tools.ietf.org/html/rfc5545#section-3.1 .
// This is achieved by enforcing the secuence "\r\n " after more
// than 75 written bytes without a newline
// Note that this implementation is considered very simple and may
// Add the linebreak in the middle of a UTF-8 sequence.
func NewWriter(w io.Writer) io.Writer {
	return &writer{w: w}
}
