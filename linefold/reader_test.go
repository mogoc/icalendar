package linefold

import (
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReader(t *testing.T) {
	d := []struct {
		In  string
		Out string
	}{
		{"\r\n ", ""},
		{"a\r", "a\r"},
	}
	for _, c := range d {
		d, err := ioutil.ReadAll(NewReader(strings.NewReader(c.In)))
		if assert.NoError(t, err, "input: %v", c.In) {
			assert.Equal(t, c.Out, string(d), "input: %v", c.In)
		}
	}
}
