package lexer

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mogoc/icalendar/linefold"
)

func testFile(t *testing.T, path string, correct bool) {
	f, err := os.Open(path)
	assert.NoError(t, err)
	defer f.Close()

	first, err := LexAll(NewLexer(linefold.NewReader(f)))
	if correct {
		if assert.NoError(t, err) {
			assert.NotEqual(t, 0, len(first))
		}
	} else {
		assert.Error(t, err)
		return
	}
	buff := bytes.NewBuffer(make([]byte, 0))
	err = WriteAll(NewWriter(buff), first)
	assert.NoError(t, err)
	second, err := LexAll(NewLexer(linefold.NewReader(buff)))
	if assert.NoError(t, err) {
		assert.Equal(t, first, second)
	}
}

func TestTestData(t *testing.T) {
	filepath.Walk("../testdata/", func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == ".ics" || filepath.Ext(path) == ".ifb" {
			correct := true
			if strings.Contains(info.Name(), "_incorrect") {
				correct = false
			}
			t.Run(info.Name(), func(t *testing.T) {
				testFile(t, path, correct)
			})
		}
		return nil
	})
}

func TestWriter(t *testing.T) {
	buff := bytes.NewBuffer([]byte{})
	w := NewWriter(buff)
	_, err := w.Write(&Expression{"TEsT-1", []byte("aöasdje9p34o23#+"), Parameters{
		"test": [][]byte{[]byte(`somethingweird`)},
	}})
	assert.NoError(t, err)
	_, err = w.Write(&Expression{"TEsT-1", []byte("aöasdje9p34o23#+"), Parameters{
		"te*st": [][]byte{[]byte(`some;thing:weird,`)},
	}})
	assert.Error(t, err)
	_, err = w.Write(&Expression{"TEsT-1", []byte("aöasdje9p34o23#+"), Parameters{
		"test": [][]byte{[]byte(`s"ome;thing:weird,`)},
	}})
	assert.Error(t, err)
	_, err = w.Write(&Expression{"T_sT-1", []byte("aöasdje9p34o23#+"), Parameters{
		"test": [][]byte{[]byte(`some;thing:weird,`)},
	}})
	assert.Error(t, err)
}
