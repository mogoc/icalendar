package lexer

import (
	"bytes"
	"errors"
	"io"
	"strings"
)

// errors
var (
	ErrIlQuoteCharInID       = errors.New(`illegal character " in identifier-value`)
	ErrIlQuoteCharInParamID  = errors.New(`illegal character " in parameter-identifier-value`)
	ErrIlQuoteCharInParamVal = errors.New(`illegal character " in parameter-value`)
)

// Writer takes an []*Expression and writes the given
// expression's text-representation in iCalendar format
// to the underlying io.Writer when the Write() function
// is called
type Writer interface {
	Write(*Expression) (int, error)
}

type writer struct {
	w io.Writer
}

// NewWriter returns a pointer to this package's
// implementation of a lexer.Writer
func NewWriter(w io.Writer) Writer {
	return &writer{w}
}

// Write writes the given expression's text-representation
// in iCalendar format to the underlying io.Writer
func (w *writer) Write(e *Expression) (n int, err error) {
	if !isValidIdentifier(e.Identifier) {
		return 0, ErrIlQuoteCharInID
	}
	c, err := w.w.Write([]byte(e.Identifier))
	n += c
	if err != nil {
		return n, err
	}
	for name, p := range e.Parameters {
		if !isValidIdentifier(name) {
			return n, ErrIlQuoteCharInParamID
		}
		c, err = w.w.Write([]byte(";" + name + "="))
		n += c
		if err != nil {
			return n, err
		}
		for i, v := range p {
			if bytes.Contains(v, []byte{'"'}) {
				return n, ErrIlQuoteCharInParamVal
			} else if bytes.Contains(v, []byte{':'}) || bytes.Contains(v, []byte{';'}) || bytes.Contains(v, []byte{','}) {
				c, err = w.w.Write([]byte{'"'})
				n += c
				if err != nil {
					return n, err
				}
				c, err = w.w.Write(v)
				n += c
				if err != nil {
					return n, err
				}
				c, err = w.w.Write([]byte{'"'})
			} else {
				c, err = w.w.Write(v)
			}
			n += c
			if err != nil {
				return n, err
			}
			if i+1 < len(p) {
				c, err = w.w.Write([]byte(","))
				n += c
				if err != nil {
					return n, err
				}
			}
		}
	}
	c, err = w.w.Write([]byte{':'})
	n += c
	if err != nil {
		return n, err
	}
	c, err = w.w.Write(e.Value)
	n += c
	if err != nil {
		return n, err
	}
	c, err = w.w.Write([]byte{'\r', '\n'})
	n += c
	if err != nil {
		return n, err
	}
	return n, nil
}

// WriteAll processes all expressions and writes the according text-represenation
func WriteAll(w Writer, es []*Expression) error {
	for _, e := range es {
		_, err := w.Write(e)
		if err != nil {
			return err
		}
		es = append(es, e)
	}
	return nil
}

func isValidIdentifier(i string) bool {
	ib := []byte(i)
	for _, b := range ib {
		if !strings.Contains(identifierChars, string(b)) {
			return false
		}
	}
	return true
}
