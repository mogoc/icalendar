package lexer

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"regexp"
	"strings"
)

// errors
var (
	ErrMatchExp    = errors.New("error while matching at expression level")
	ErrMatchParams = errors.New("error while matching at parameters level")
	ErrMatchParam  = errors.New("error while matching at parameter level")
)

// character groups of the iCalendar syntax
const (
	identifierChars   = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-"
	illegalValueChars = `";:,`
)

// line regex splits a line into three parts: the identifier, the optional
// parameter-construct and the value
var line = regexp.MustCompile(`^((?:[[:alnum:]]|-)+)((?:"[^"]*"|[^:"]*)*):(.*)$`)

// parameters regex splits the parameter-construct into single
// parameters
var parameters = regexp.MustCompile(`;((?:[[:alnum:]]|-)+=(?:"[^"]*"|[^;:])*)`)

// Parameters holds a set of parameters for an Expression
type Parameters map[string][][]byte

// Expression can hold any iCalendar expression
type Expression struct {
	// mandatory
	Identifier string
	Value      []byte
	// optional
	Parameters Parameters
}

// Lexer processes a iCalendar document provided as an io.Reader
// line by line
type Lexer interface {
	Next() (*Expression, error)
}

// lexer implements Lexer
type lexer struct {
	r *bufio.Reader
}

// NewLexer returns a pointer to this package's
// implementation of a lexer.Lexer
func NewLexer(r io.Reader) Lexer {
	return &lexer{bufio.NewReader(r)}
}

// Next processes the next line and returns the according Expression
func (l *lexer) Next() (*Expression, error) {
	ln, _, err := l.r.ReadLine()
	if err != nil {
		if err != io.EOF {
			return nil, err
		}
		if len(ln) == 0 {
			return nil, nil
		}
		return nil, io.ErrUnexpectedEOF
	}
	ex, err := ReadLine(ln)
	if err != nil {
		return nil, err
	}
	return &ex, nil
}

// LexAll processes all lines and returns the according Expressions
func LexAll(l Lexer) ([]*Expression, error) {
	es := make([]*Expression, 0)
	for {
		e, err := l.Next()
		if err != nil {
			return es, err
		}
		if e == nil {
			return es, nil
		}
		es = append(es, e)
	}
}

// ReadLine takes a string and tries to map it to an Expression
func ReadLine(l []byte) (ex Expression, err error) {
	lm := line.FindSubmatch(l)
	if lm == nil || len(lm) != 4 {
		return ex, ErrMatchExp
	}

	ex.Identifier = string(lm[1])
	ex.Value = lm[3]

	if len(lm[2]) > 0 {
		ex.Parameters = make(Parameters)
		psm := parameters.FindAllSubmatch(lm[2], -1)
		if psm == nil {
			return ex, ErrMatchParams
		}
		var length int
		for _, m := range psm {
			for i := 1; i < len(m); i++ {
				length += len([]byte(m[i])) + 1
			}
		}
		if length != len([]byte(lm[2])) {
			return ex, ErrMatchParams
		}

		for _, m := range psm {
			for i := 1; i < len(m); i++ {

				pm := processParameter(m[i])
				if pm == nil || len(pm) < 3 {
					return ex, ErrMatchParam
				}

				param := make([][]byte, 0)
				for j := 2; j < len(pm); j++ {
					if len(pm[j]) > 0 {
						param = append(param, pm[j])
					}
				}

				ex.Parameters[string(pm[1])] = param
			}
		}
	}

	return
}

func processParameter(p []byte) (params [][]byte) {
	params = make([][]byte, 1)
	params[0] = p
	// mode represents the algorithm's status (0 = identifier,
	// 1 = parameter, 2 = normal-parameter, 3 = quoted-string-parameter)
	var mode int
	buf := &bytes.Buffer{}
	for _, b := range p {
		switch mode {
		case 0:
			if !strings.Contains(identifierChars, string(b)) {
				if b == '=' {
					params = append(params, append(make([]byte, 0, buf.Len()), buf.Bytes()...))
					buf.Reset()
					mode = 1
					continue
				} else {
					return nil
				}
			}
		case 1:
			if b == '"' {
				mode = 3
				continue
			} else if b == ',' {
				continue
			} else if strings.Contains(illegalValueChars, string(b)) {
				return nil
			} else {
				mode = 2
			}
		case 2:
			if strings.Contains(illegalValueChars, string(b)) {
				if b == ',' {
					params = append(params, append(make([]byte, 0, buf.Len()), buf.Bytes()...))
					buf.Reset()
					mode = 1
					continue
				} else {
					return nil
				}
			}
		case 3:
			if b == '"' {
				params = append(params, append(make([]byte, 0, buf.Len()), buf.Bytes()...))
				buf.Reset()
				mode = 1
				continue
			}
		}
		err := buf.WriteByte(b)
		if err != nil {
			return nil
		}
	}
	if mode == 1 || mode == 2 {
		if buf.Len() != 0 {
			params = append(params, append(make([]byte, 0, buf.Len()), buf.Bytes()...))
		}
		return
	}
	return nil
}
